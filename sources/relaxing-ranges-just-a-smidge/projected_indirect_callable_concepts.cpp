template< class F, class I, class P >
  concept projected_reference_invocable =
    reference_projection<P, I> &&
    copy_constructible<F> &&
    invocable<F&, reference_projection_t<I, P>>;

template< class F, class I, class P >
  concept projected_value_invocable =
    value_projection<P, I> &&
    projected_reference_invocable<F, I, P> &&
    invocable<F&, value_projection_t<I, P>> &&
    invocable<F&, common_projection_t<I, P>> &&
    common_reference_with<
      invoke_result_t<F&, value_projection_t<I, P>>,
      invoke_result_t<F&, common_projection_t<I, P>>>;

template< class F, class I, class P >
  concept regular_projected_reference_invocable =
    reference_projection<P, I> &&
    copy_constructible<F> &&
    regular_invocable<F&, reference_projection_t<I, P>>;

template< class F, class I, class P >
  concept regular_projected_value_invocable =
    value_projection<P, I> &&
    regular_projected_reference_invocable<F, I, P> &&
    regular_invocable<F&, value_projection_t<I, P>> &&
    regular_invocable<F&, common_projection_t<I, P>> &&
    common_reference_with<
      invoke_result_t<F&, value_projection_t<I, P>>,
      invoke_result_t<F&, common_projection_t<I, P>>>;

