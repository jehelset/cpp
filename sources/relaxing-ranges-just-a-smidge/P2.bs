<pre class='metadata' highlight=cpp>
Title: Relaxing Ranges Just A Smidge
Shortname: P2609
Audience: SG9, LEWG
Status: P
Revision: 2
Group: WG21
!Subgroup: SG9
!Source: <a href="https://gitlab.com/jehelset/cpp/-/blob/main/sources/relaxing-ranges-just-a-smidge/P2.bs">https://gitlab.com/jehelset/cpp/-/blob/main/sources/relaxing-ranges-just-a-smidge/P2.bs</a>
URL: https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/index.html
Editor: John Eivind Helset, private@jehelset.no
Date: 2023-01-21
Markup Shorthands: markdown yes
No abstract: true
Toggle Diffs: yes
</pre>

<style>
table, th, td { border:1px solid grey; }
</style>

Abstract {#abstract}
========

Ranges algorithms that take a function and a projection should, in the unary case, constrain the
function to enable:

<pre>
  iter_value_t&lt;It&gt; x = *it;
  f(proj(x));
</pre>
Instead they are constrained to allow:
<pre>
  iter_value_t&lt;projected&lt;I,Proj&gt;&gt; u = proj(*it);
  f(u);
</pre>

And likewise in the binary case. This is caused by the composition of indirect callable concepts
with `projected`, seen for example in the constraints of `ranges::for_each` as
`indirect_unary_invocable<projected<I,P>>`.

A fix is proposed that introduces a type-trait and makes a slight change to the definitions of the
indirect callable concepts, as well as `iter_common_reference_t`. The fix is a slight relaxation of
the algorithmic constraints in ranges that does not break ABI.

Revisions {#revisions}
=========

R1  {#revisions-R1}
---

- range-v3 PR now C++14 friendly, gcc implementation now exposition-only.
- Cleanup wording.
- Make `indirect_value_t` exposition-only.
- Add note on P2248R4.
- Bump `__cpp_lib_ranges` feature-test macro.

R2  {#revisions-R2}
---

- Fixup incorrect edits, and exposition-only syntax.
- Ensure <i>`indirect-value-t`</i> can handle nested `projected`s.

Problem {#problem}
=======

In [[Niebler2015]] Eric Niebler explains that the constraints put on the predicate of
`ranges::unique_copy` must be strong enough to allow the algorithm to invoke it with a reference
formed to a copy of an element:

<blockquote>
Sometimes the algorithms call the predicates with references, sometimes with values, and sometimes — like unique_copy — with a mix of both.
</blockquote>

That iteration of `ranges::unique_copy` did not have projections. The current iteration of
`ranges::unique_copy` has projections, and is specified as:
<pre>
template&lt;input_range R, weakly_incrementable O, class Proj = identity,
         indirect_equivalence_relation&lt;projected&lt;iterator_t&lt;R&gt;, Proj&gt;&gt; C = ranges::equal_to&gt;
  requires indirectly_copyable&lt;iterator_t&lt;R&gt;, O&gt; &amp;&amp;
           (forward_iterator&lt;iterator_t&lt;R&gt;&gt; ||
            (input_iterator&lt;O&gt; &amp;&amp; same_as&lt;range_value_t&lt;R&gt;, iter_value_t&lt;O&gt;&gt;) ||
            indirectly_copyable_storable&lt;iterator_t&lt;R&gt;, O&gt;)
  constexpr ranges::unique_copy_result&lt;borrowed_iterator_t&lt;R&gt;, O&gt;
    ranges::unique_copy(R&amp;&amp; r, O result, C comp = {}, Proj proj = {});
</pre>

In [[Revzin2022]], Barry Revzin writes that projections are function adaptors, and do not change an
algorithm. Projections as function adaptors is also discussed in <a
href="https://github.com/ericniebler/range-v3/issues/211">ranges-v3/issues/233</a>. In the unary
case, projection is reduced to composition:

<blockquote>
Put differently, every algorithm in the standard library that accepts a unary predicate and a projection, such that we have `algo(..., pred, proj)` can have its projection dropped without any loss of functionality because users can do the function composition themselves and invoke it as `algo(..., hof::compose(pred, proj)))`.
</blockquote>

Yet the constraints put on the triplet of iterator, function and projection fail to express
projections as function adaptors. In the unary case they do not express
`compose(f,proj)`. `ranges::for_each`, for example, takes a unary function
`indirectly_unary_invocable<projected<I, Proj>> Fun`. The definitions of
`indirectly_unary_invocable` and `projected` are:

<pre class=include-code>
  path: indirect_callable_concepts.cpp
  highlight: c++
  show: 39-48
</pre>
<pre class=include-code>
  path: class_template_projected.cpp
  highlight: c++
  show: 29-34
</pre>

Where `indirect_result_t` is:

<pre class=include-code>
  path: indirect_callable_concepts.cpp
  highlight: c++
  show: 62-65
</pre>

The problematic constraint is `invocable<F &, iter_value_t<projected<I, Proj>> &>`, which becomes
apparent when inlining the definition of `iter_value_t`:

<pre>
invocable&lt;F &amp;, remove_cvref_t&lt;invoke_result_t&lt;Proj &amp;, iter_reference_t&lt;I&gt;&gt; &amp;&gt;
</pre>

The code that this constraint is supposed to enable, and the code it actually enables, for copying
and a referencing algorithm is:

<table>
<tr><th>Algorithm</th><th>Expected</th><th>Actual</th></tr>
<tr>
  <td>Referencing</td>
  <td>
    <pre>f(proj(*it))</pre>
  </td>
  <td>
    <pre>f(proj(*it))</pre></td></tr>
<tr>
  <td>Copying</td>
  <td>
    <pre>
      iter_value_t&lt;It&gt; v = *it;
      f(proj(v));
    </pre>
  </td>
  <td>
    <pre>
      iter_value_t&lt;projected&lt;I,P&gt;&gt; u = proj(*it);
      f(u);
    </pre>
  </td>
</tr>
</table>

None of the algorithms in ranges are constrained to contain `f(u)`. They do not require a copyable
or movable `u`. Requiring copyability or movability would not be enough. One would also need to
somehow deal with the validity of `u`. There is no guarantee that the validity of `u` is not tied to
the element `*it`.  This was discussed in <a
href="https://github.com/ericniebler/range-v3/issues/148">ranges-v3/issues/148</a>,
and partially resolved by [[P0740R0]]. 

The actual consequence on user code seems small, as the malformed syntactic requirement of the
constraint is often naturally covered. Valid, but perhaps rare, code will not compile or need
work-arounds. In particular projections returning move-only types become problematic:

<pre>
std::ranges::for_each(
  std::views::iota(0, 5),
  [](std::unique_ptr&lt;int&gt; v){ //Can't be invoked with std::unique_ptr&lt;int&gt; &amp;
    std::cout &lt;&lt; &ast; v &lt;&lt; std::endl;
  },
  [](int v){
    return std::make_unique&lt;int&gt;(v);
  });
</pre>

Proposal {#proposal}
========

The proposal aims at a minimal fix. First an exposition-only type-trait is defined that
disambiguates between the `value_type` of an iterator and a projection:

<pre>
template&lt;indirectly_readable I&gt;
using <i>indirect-value-t</i> = see below; // exposition only
</pre>

The implementation technique is unspecified, but <i>`indirect-value-t<I>`</i> must be
`iter_value_t<I> &` for an iterator and <code>invoke_result_t&lt;Proj &amp;,
<i>indirect-value-t</i>&lt;Iter&gt;&gt;</code> for `projected<Proj,Iter>`. The indirect
callable concepts, as well as `iter_common_reference_t` are now redefined to use
<i>`indirect-value-t`</i> instead of `iter_value_t`, showcased here for
`indirectly_unary_invocable`:

<pre>
template&lt; class F, class I &gt;
  concept indirectly_unary_invocable =
    indirectly_readable&lt;I&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    invocable&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt; &amp;&amp;
    invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
    invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
    common_reference_with&lt;
      invoke_result_t&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt;,
      invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;
</pre>

Note: In the event [[P2248R4]] is merged before this proposal, the `projected_value_t` alias template of that
proposal should be redefined in terms of <code>remove_cvref_t&lt;<i>indirect-value-t</i>&lt;I&gt;&gt;</code> as they compute
the same type and have the same semantics.

Implementation {#implementation}
==============

This proposal has currently been implemented and tested with both <a
href="https://github.com/jehelset/range-v3/tree/relax-indirect-callable-concepts">ranges-v3</a> and
<a
href="https://gitlab.com/jehelset/gcc/-/tree/jehelset/relaxing-ranges-just-a-smidge">libstdc++</a>
(on the gcc-12 release branch) without any issues.

ranges-v3 {#implementation-ranges-v3}
---------

ranges-v3 has a firewalled `projected` (as proposed in [[P2538R1]]). Here the implementation uses a partial specialization that
is constrained on the existence of a "secret" nested alias declaration. 

libstdc++ {#implementation-libstdc++}
---------

libstdc++ has not yet firewalled `projected`, so here a simple partial specialization was used.
<pre class=include-code>
  path: libstdc++.cpp
  highlight: c++
  show: 1-14
</pre>

Wording {#wording}
=======

17.3.2 Header &lt;version&gt; synopsis <a href="https://wg21.link/version.synopsis">[version.synopsis]</a> {#wording.version.synopsis}
-------------------------------------- 

Bump `__cpp_lib_ranges` with the value specified as usual (year and month of adoption).

<pre>
  #define __cpp_lib_ranges YYYYMML  // also in
      // &lt;algorithm&gt;,&lt;functional&gt;,&lt;iterator&gt;,&lt;memory&gt;,&lt;ranges&gt;
</pre>

23.2 Header &lt;iterator&gt; synopsis <a href="https://wg21.link/iterator.synopsis">[iterator.synopsis]</a> {#wording.iterator.synopsis}
------------------------------------- 

<pre>
  // [iterator.concepts], iterator concepts
  // [iterator.concept.readable], concept indirectly_­readable
  template&lt;class In&gt;
    concept indirectly_readable = see below;

  <ins>template&lt;indirectly_­readable T&gt;
    using <i>indirect-value-t</i> = see below; // exposition only</ins>

  template&lt;indirectly_­readable T&gt;
    using iter_common_reference_t =
      common_reference_t&lt;iter_reference_t&lt;T&gt;, <ins><i>indirect-value-t&lt;T&gt;</i></ins><del>iter_value_t&lt;T&gt;&amp;</del>&gt;;
</pre>

23.3.2.4 Indirect callable traits [indirectcallable.traits]  {#wording.indirectcallable.traits}
---------------------------------

1. To implement algorithms taking projections, it is necessary to determine the projected type of an iterators value type. The exposition-only alias template `indirect-value-t<T>` denotes
      - <code>invoke_result_t&lt;Proj&amp;, <i>indirect-value-t</i>&lt;I&gt;&gt;</code> if `T` names `projected<I, Proj>` 
      - and `iter_value_t<T> &` otherwise.

23.3.6.4 Indirect callables <a href="https://wg21.link/indirectcallable.indirectinvocable">[indirectcallable.indirectinvocable]</a>  {#wording.indirectcallable.indirectinvocable}
---------------------------

<pre>
namespace std {
  template&lt; class F, class I &gt;
    concept indirectly_unary_invocable =
      indirectly_readable&lt;I&gt; &amp;&amp;
      copy_constructible&lt;F&gt; &amp;&amp;
      invocable&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt; &amp;&amp;
      invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
      invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
      common_reference_with&lt;
	invoke_result_t&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt;,
	invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;

  template&lt;class F, class I&gt;
    concept indirectly_regular_unary_invocable =
      indirectly_readable&lt;I&gt; &amp;&amp;
      copy_constructible&lt;F&gt; &amp;&amp;
      regular_invocable&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt; &amp;&amp;
      regular_invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
      regular_invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
      common_reference_with&lt;
	invoke_result_t&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt;,
	invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;

  template&lt;class F, class I&gt;
    concept indirect_unary_predicate =
      indirectly_readable&lt;I&gt; &amp;&amp;
      copy_constructible&lt;F&gt; &amp;&amp;
      predicate&lt;F&amp;, <ins><i>indirect-value-t&lt;I&gt;</i></ins><del>iter_value_t&lt;I&gt;&amp;&gt;</del> &amp;&amp;
      predicate&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
      predicate&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt;;

  template&lt;class F, class I1, class I2&gt;
    concept indirect_binary_predicate =
      indirectly_readable&lt;I1&gt; &amp;&amp; indirectly_readable&lt;I2&gt; &amp;&amp;
      copy_constructible&lt;F&gt; &amp;&amp;
      predicate&lt;F&amp;, <ins><i>indirect-value-t&lt;I1&gt;</i></ins><del>iter_value_t&lt;I1&gt;&amp;</del>, <ins><i>indirect-value-t&lt;I2&gt;</i></ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
      predicate&lt;F&amp;, <ins><i>indirect-value-t&lt;I1&gt;</i></ins><del>iter_value_t&lt;I1&gt;&amp;</del>, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
      predicate&lt;F&amp;, iter_reference_t&lt;I1&gt;, <ins><i>indirect-value-t&lt;I2&gt;</i></ins><del>iter_value_t&lt;I2&gt;&amp;&gt;</del> &amp;&amp;
      predicate&lt;F&amp;, iter_reference_t&lt;I1&gt;, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
      predicate&lt;F&amp;, iter_common_reference_t&lt;I1&gt;, iter_common_reference_t&lt;I2&gt;&gt;;

  template&lt;class F, class I1, class I2 = I1&gt;
    concept indirect_equivalence_relation =
      indirectly_readable&lt;I1&gt; &amp;&amp; indirectly_readable&lt;I2&gt; &amp;&amp;
      copy_constructible&lt;F&gt; &amp;&amp;
      equivalence_relation&lt;F&amp;, <ins><i>indirect-value-t&lt;I1&gt;</i></ins><del>iter_value_t&lt;I1&gt;&amp;</del>, <ins><i>indirect-value-t&lt;I2&gt;</i></ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
      equivalence_relation&lt;F&amp;, <ins><i>indirect-value-t&lt;I1&gt;</i></ins><del>iter_value_t&lt;I1&gt;&amp;</del>, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
      equivalence_relation&lt;F&amp;, iter_reference_t&lt;I1&gt;, <ins><i>indirect-value-t&lt;I2&gt;</i></ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
      equivalence_relation&lt;F&amp;, iter_reference_t&lt;I1&gt;, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
      equivalence_relation&lt;F&amp;, iter_common_reference_t&lt;I1&gt;, iter_common_reference_t&lt;I2&gt;&gt;;

  template&lt;class F, class I1, class I2 = I1&gt;
    concept indirect_strict_weak_order =
      indirectly_readable&lt;I1&gt; &amp;&amp; indirectly_readable&lt;I2&gt; &amp;&amp;
      copy_constructible&lt;F&gt; &amp;&amp;
      strict_weak_order&lt;F&amp;, <ins><i>indirect-value-t&lt;I1&gt;</i></ins><del>iter_value_t&lt;I1&gt;&amp;</del>, <ins><i>indirect-value-t&lt;I2&gt;</i></ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
      strict_weak_order&lt;F&amp;, <ins><i>indirect-value-t&lt;I1&gt;</i></ins><del>iter_value_t&lt;I1&gt;&amp;</del>, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
      strict_weak_order&lt;F&amp;, iter_reference_t&lt;I1&gt;, <ins><i>indirect-value-t&lt;I2&gt;</i></ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
      strict_weak_order&lt;F&amp;, iter_reference_t&lt;I1&gt;, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
      strict_weak_order&lt;F&amp;, iter_common_reference_t&lt;I1&gt;, iter_common_reference_t&lt;I2&gt;&gt;;
</pre>

Acknowledgements {#acknowledgements}
================

The author would like to thank Barry Revzin, Christopher Di Bella, Arthur O'Dwyer, Hui Xie
and Tim Song for guidance.

<pre class=biblio>
{
"P2248R4": {
  "title": "Enabling list-initialization for algorithms",
  "authors": ["Giuseppe D'Angelo"],
  "href": "https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2248r4.html"
},
"P2538R1": {
  "title": "ADL-proof std::projected",
  "authors": ["Arthur O'Dwyer", "Casey Carter"],
  "href": "http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2538r1.html"
},
"P0740R0": {
  "title": "Ranges TS \"Immediate\" Issues from the July 2017 (Toronto) meeting",
  "authors": [ "Casey Carter" ],
  "href": "https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0740r0.html"
},
"Revzin2022": {
  "title": "Projections are Function Adaptors",
  "authors": ["Barry Revzin"],
  "href": "https://brevzin.github.io/c++/2022/02/13/projections-function-adaptors/"
},
"Niebler2015": {
  "title": "Iterators++, Part 3",
  "authors": ["Eric Niebler"],
  "href": "https://ericniebler.com/2015/03/03/iterators-plus-plus-part-3/"
}
}
</pre>
