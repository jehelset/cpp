//del
template<indirectly_readable T>
  using iter_common_reference_t =
    common_reference_t<iter_reference_t<T>, iter_value_t<T>&>;

//ins
template<indirectly_readable T>
  using iter_common_reference_t =
  common_reference_t<iter_reference_t<T>, indirect_value_t<T>>;
