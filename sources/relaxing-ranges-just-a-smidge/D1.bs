<pre class='metadata' highlight=cpp>
Title: Relaxing Ranges Just A Smidge
Shortname: DXXXX
Revision: 1
Audience: SG9
Status: D
Group: WG21
!Source: <a href="https://gitlab.com/jehelset/cpp/-/blob/main/sources/relaxing-ranges-just-a-smidge/D1.bs">https://gitlab.com/jehelset/cpp/-/blob/main/sources/relaxing-ranges-just-a-smidge/D1.bs</a>
URL: https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/D1.html
Editor: John Eivind Helset, private@jehelset.no
Date: 2022-04-17
Markup Shorthands: markdown yes
No abstract: true
Toggle Diffs: yes
</pre>

<style>
table, th, td { border:1px solid grey; }
</style>

Abstract {#abstract}
========

Ranges algorithms that take a function and a projection should put constraints on the function to allow:
<pre>
  auto x = proj(*it);
  f(proj(*it));
</pre>
Instead it is constrained to allow:
<pre>
  auto u = proj(*it);
  f(u);
</pre>

This is caused by the indirect callable concepts using the construction `iter_value_t<projected<I,
Proj>> &`. A fix is proposed that requires a type-trait and a slight change to the indirect callable
concepts.

Revisions {#revisions}
=========

R1  {#revisions-R1}
---

- Drop unecessary invocability
- Add solution for nonsensical invocability
- Clean up exposition

Problem {#problem}
=======

In the discussion of `ranges::unique_copy` in [[Niebler2015]] Eric Niebler explains that the
constraint put on the comparator by `ranges::unique_copy` must be strong enough to allow the
algorithm to invoke with a reference formed to a copy of an element:

<blockquote>
Sometimes the algorithms call the predicates with references, sometimes with values, and sometimes — like unique_copy — with a mix of both.
</blockquote>

That iteration of `ranges::unique_copy` did not have projections. The current iteration of
`ranges::unique_copy` has projections, and is specified as:
<pre>
template&lt;input_range R, weakly_incrementable O, class Proj = identity,
         indirect_equivalence_relation&lt;projected&lt;iterator_t&lt;R&gt;, Proj&gt;&gt; C = ranges::equal_to&gt;
  requires indirectly_copyable&lt;iterator_t&lt;R&gt;, O&gt; &amp;&amp;
           (forward_iterator&lt;iterator_t&lt;R&gt;&gt; ||
            (input_iterator&lt;O&gt; &amp;&amp; same_as&lt;range_value_t&lt;R&gt;, iter_value_t&lt;O&gt;&gt;) ||
            indirectly_copyable_storable&lt;iterator_t&lt;R&gt;, O&gt;)
  constexpr ranges::unique_copy_result&lt;borrowed_iterator_t&lt;R&gt;, O&gt;
    ranges::unique_copy(R&amp;&amp; r, O result, C comp = {}, Proj proj = {});
</pre>

In [[Revzin2022]], Barry Revzin writes that projections are function adaptors, and do not change an
algorithm. In the unary case, projection is reduced to composition:

<blockquote>
Put differently, every algorithm in the standard library that accepts a unary predicate and a projection, such that we have `algo(..., pred, proj)` can have its projection dropped without any loss of functionality because users can do the function composition themselves and invoke it as `algo(..., hof::compose(pred, proj)))`.
</blockquote>

The constraints put on the triplet of iterator, function and projection fail to express projections
as function adapters, and in the unary case do not express `compose(f,proj)`. `ranges::for_each`, for
example, takes a unary function `indirectly_unary_invocable<projected<I, Proj>> Fun`. The
definitions of `indirectly_unary_invocable` and `projected` are:
<pre class=include-code>
  path: indirect_callable_concepts.cpp
  highlight: c++
  show: 40-50 
</pre>
<pre class=include-code>
  path: class_template_projected.cpp
  highlight: c++
  show: 29-34
</pre>
Where `indirect_result_t` is:
<pre class=include-code>
  path: indirect_callable_concepts.cpp
  highlight: c++
  show: 63-66
</pre>

The problematic constraint is `invocable<F &, iter_value_t<projected<I, Proj>> &>`, which becomes
apparent when inlining the definition of `iter_value_t`:

<pre>
invocable&lt;F &amp;, remove_cvref_t&lt;invoke_result_t&lt;Proj &amp;, iter_reference_t&lt;I&gt;&gt; &amp;&gt;
</pre>

The code that this constraint is supposed to enable, and the code it actually enables,
for copying and a referencing algorithm is:

<table>
<tr><th>Algorithm</th><th>Expected</th><th>Actual</th></tr>
<tr>
  <td>Referencing</td>
  <td>
    <pre>f(proj(*it))</pre>
  </td>
  <td>
    <pre>f(proj(*it))</pre></td></tr>
<tr>
  <td>Copying</td>
  <td>
    <pre>
      auto v = *it;
      f(proj(v));
    </pre>
  </td>
  <td>
    <pre>
      auto u = proj(*it);
      f(u);
    </pre>
  </td>
</tr>
</table>

None of the algorithms in ranges are constrained to contain `f(u)`. They do not require a copyable
`u`. Requiring copyability would not be enough. One would also need to somehow deal with the
validity of `u`. There is no guarantee that the validity of `u` is not tied to the element `*it`.

`invocable<F &, iter_value_t<projected<I, Proj>> &>` seems like a mistake.

Impact {#introduction-impact}
------

The problematic syntax is `iter_value_t<projected<I, Proj>> &`. This is commonly found in the
indirect callable concepts which are used to constrain the algorithms in ranges. As a result these
concepts fail to correctly express their intended semantics.

The actual consequence on user code seems small. Valid, but perhaps rare, code will not compile or
need work-arounds. The syntactic requirement of the constraint is often naturally covered, but
projections returning move-only types might require work-arounds, depending on the function:

<pre>
std::ranges::for_each(
  std::views::iota(0, 5),
  [](std::unique_ptr&lt;int&gt; v){ //Can't be invoked with std::unique_ptr&lt;int&gt; &amp;
    std::cout &lt;&lt; &ast; v &lt;&lt; std::endl;
  },
  [](int v){
    return std::make_unique<int>(v);
  });
</pre>

Proposal {#proposal}
========

The proposal aims at a minimal fix. First a type-trait is defined so that one can disambiguate
between the indirect `value_type` of an iterator and a projection:

<pre class=include-code>
  path: indirect_callable_concepts.cpp
  highlight: c++
  show: 30-38
</pre>
This is partially specialized for `projected`:
<pre class=include-code>
  path: class_template_projected.cpp
  highlight: c++
  show: 24-28
</pre>

Finally the indirect callable concepts are redefined to use `indirect_value_t` instead of
`iter_value_t`, showcased here for `indirectly_unary_invocable`:

<pre>
template&lt; class F, class I &gt;
  concept indirectly_unary_invocable =
    indirectly_readable&lt;I&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    invocable&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt; &amp;&amp;
    invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
    invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
    common_reference_with&lt;
      invoke_result_t&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt;,
      invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;
</pre>

Wording {#wording}
=======

Indirectly readable traits {#wording-indirectly_readable_traits}
--------------------------

<pre class=include-code>
  path: indirect_callable_concepts.cpp
  highlight: c++
  show: 30-38
</pre>

Class template projected {#wording-class_template_projected}
------------------------

<pre class=include-code>
  path: class_template_projected.cpp
  highlight: c++
  show: 24-28
</pre>

Indirectly callable concepts {#wording-indirectly_callable_concepts}
----------------------------

<pre>
template&lt; class F, class I &gt;
  concept indirectly_unary_invocable =
    indirectly_readable&lt;I&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    invocable&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt; &amp;&amp;
    invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
    invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
    common_reference_with&lt;
      invoke_result_t&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt;,
      invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;
</pre>

<pre>
template&lt;class F, class I&gt;
  concept indirectly_regular_unary_invocable =
    indirectly_readable&lt;I&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    regular_invocable&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt; &amp;&amp;
    regular_invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
    regular_invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
    common_reference_with&lt;
      invoke_result_t&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;</del>&gt;,
      invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;
</pre>

<pre>
template&lt;class F, class I&gt;
  concept indirect_unary_predicate =
    indirectly_readable&lt;I&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    predicate&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I&gt;&amp;&gt;</del> &amp;&amp;
    predicate&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
    predicate&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt;;
</pre>

<pre>
template&lt;class F, class I1, class I2&gt;
  concept indirect_binary_predicate =
    indirectly_readable&lt;I1&gt; &amp;&amp; indirectly_readable&lt;I2&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    predicate&lt;F&amp;, <ins>indirect_value_t&lt;I1&gt;</ins><del>iter_value_t&lt;I1&gt;&amp;</del>, <ins>indirect_value_t&lt;I1&gt;</ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
    predicate&lt;F&amp;, <ins>indirect_value_t&lt;I1&gt;</ins><del>iter_value_t&lt;I1&gt;&amp;</del>, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
    predicate&lt;F&amp;, iter_reference_t&lt;I1&gt;, <ins>indirect_value_t&lt;I1&gt;</ins><del>iter_value_t&lt;I2&gt;&amp;&gt;</del> &amp;&amp;
    predicate&lt;F&amp;, iter_reference_t&lt;I1&gt;, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
    predicate&lt;F&amp;, iter_common_reference_t&lt;I1&gt;, iter_common_reference_t&lt;I2&gt;&gt;;
</pre>

<pre>
template&lt;class F, class I1, class I2 = I1&gt;
  concept indirect_equivalence_relation =
    indirectly_readable&lt;I1&gt; &amp;&amp; indirectly_readable&lt;I2&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    equivalence_relation&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I1&gt;&amp;</del>, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
    equivalence_relation&lt;F&amp;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I1&gt;&amp;</del>, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
    equivalence_relation&lt;F&amp;, iter_reference_t&lt;I1&gt;, <ins>indirect_value_t&lt;I&gt;</ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
    equivalence_relation&lt;F&amp;, iter_reference_t&lt;I1&gt;, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
    equivalence_relation&lt;F&amp;, iter_common_reference_t&lt;I1&gt;, iter_common_reference_t&lt;I2&gt;&gt;;
</pre>

<pre>
template&lt;class F, class I1, class I2 = I1&gt;
  concept indirect_strict_weak_order =
    indirectly_readable&lt;I1&gt; &amp;&amp; indirectly_readable&lt;I2&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    strict_weak_order&lt;F&amp;, <ins>indirect_value_t&lt;I1&gt;</ins><del>iter_value_t&lt;I1&gt;&amp;</del>, <ins>indirect_value_t&lt;I2&gt;</ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
    strict_weak_order&lt;F&amp;, <ins>indirect_value_t&lt;I1&gt;</ins><del>iter_value_t&lt;I1&gt;&amp;</del>, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
    strict_weak_order&lt;F&amp;, iter_reference_t&lt;I1&gt;, <ins>indirect_value_t&lt;I2&gt;</ins><del>iter_value_t&lt;I2&gt;&amp;</del>&gt; &amp;&amp;
    strict_weak_order&lt;F&amp;, iter_reference_t&lt;I1&gt;, iter_reference_t&lt;I2&gt;&gt; &amp;&amp;
    strict_weak_order&lt;F&amp;, iter_common_reference_t&lt;I1&gt;, iter_common_reference_t&lt;I2&gt;&gt;;
</pre>

Acknowledgements {#acknoweldgements}
================

The author would like to thank Barry Rezvin, Christopher Di Bella, and Arthur O'Dwyer for guidance.

<pre class=biblio>
{
"Revzin2022": {
  "title": "Projections are Function Adaptors",
  "authors": ["Barry Revzin"],
  "href": "https://brevzin.github.io/c++/2022/02/13/projections-function-adaptors/"
},
"Niebler2015": {
  "title": "Iterators++, Part 3",
  "authors": ["Eric Niebler"],
  "href": "https://ericniebler.com/2015/03/03/iterators-plus-plus-part-3/"
}
}
</pre>
