namespace __detail{

  template<typename _I>
     struct __indirect_value_impl
     { using type = iter_value_t<_I> &; };

  template<indirectly_readable _I>
    using __indirect_value_t = typename __detail::__indirect_value_impl<_I>::type;

  template<weakly_incrementable _Iter, typename _Proj>
    struct __indirect_value_impl<projected<_Iter, _Proj>>
    { using type = invoke_result_t<_Proj &,iter_value_t<_Iter> &>; };

} // namespace __detail

