P2609R0
=======

Relaxing Ranges Just a Smidge!
------------------------------

----

Problem
=======

The `std::ranges` algorithms that take an iterator, a function, and a projection are partly
misconstrained.

.. code-block:: c++

   template<input_iterator I, sentinel_for<I> S, class Proj = identity,
	    indirectly_unary_invocable<projected<I, Proj>> Fun>
   constexpr ranges::for_each_result<I, Fun>
     ranges::for_each( I first, S last, Fun f, Proj proj = {} );

This triplet constrained by the indirect callable concepts and `std::projected`.

----

Problem
=======

`indirectly_unary_invocable<F, I>` should, among other things, ensure this call is well-formed:

.. code-block:: c++

   //good block!
   iter_value_t<I> x = *it;
   f(proj(x));

However, when `I = projected<It, Proj>`, it instead ensures that this call is well-formed:

.. code-block:: c++

   //bad block!
   iter_value_t<projected<It,Proj>> u = proj(*it); 
   f(u);

In the good block the projection is a function adaptor, in the bad block the projection an
iterator adaptor.

|

----

Cause
=====

Caused by indirect callable concepts and `projected`. I here only consider
`indirectly_unary_invocable`, but the rest have the same issue:

.. code-block:: cpp
		
   template< class F, class I >
   concept indirectly_unary_invocable =
     indirectly_readable<I> &&
     copy_constructible<F> &&
     invocable<F&, iter_value_t<I>&> &&
     invocable<F&, iter_reference_t<I>> &&
     invocable<F&, iter_common_reference_t<I>> &&
     common_reference_with<
       invoke_result_t<F&, iter_value_t<I>&>,
       invoke_result_t<F&, iter_reference_t<I>>>;

This works as intended for iterators, but for `projected<It,Proj>`...

----

Cause
=====

Substituting in the definition of `iter_value_t` for `projected<It,Proj>`:

.. code-block:: cpp

   //snip...
   concept indirectly_unary_invocable =
     //snip...
     invocable<F&, remove_cvref_t<\
       invoke_result_t<Proj&, iter_reference_t<It>> &>
     //snip...

Which mirrors the bad block from earlier:
     
.. code-block:: cpp

   remove_cvref_t<invoke_result_t<Proj&,iter_reference_t<It>> \
     u = proj(*it); 
   f(u);


In the case of `projected`, `iter_value_t<I> &` forms a reference on the "result-side",
instead of the "parameter-side" of the projection!

----

Fix
===

The indirect callable concepts need to distinguish iterators from projecteds!

.. code-block:: cpp
		
   template< class F, class I >
   concept indirectly_unary_invocable =
     indirectly_readable<I> &&
     copy_constructible<F> &&
     invocable<F&, iter_value_t<I>&> &&
     invocable<F&, iter_reference_t<I>> &&
     invocable<F&, iter_common_reference_t<I>> &&
     common_reference_with<
       invoke_result_t<F&, iter_value_t<I>&>,
       invoke_result_t<F&, iter_reference_t<I>>>;

|

----

Fix
===

Introduce a type-trait `indirect_value_t` which is used instead of `iter_value_t<I> &` in all the
indirect callable concepts:

.. code-block:: cpp

   //snip
   concept indirectly_unary_invocable =
     //snip...
     invocable<F&, indirect_value_t<I>> &&
     //snip...
     common_reference_with<
       invoke_result_t<F&, indirect_value_t<I>>,
       //snip...

|
       
----

Fix
===

For iterators `indirect_value_t<I>` is:

.. code-block:: c++

   iter_value_t<I> &

For projecteds `indirect_value_t<projected<It,Proj>>` is:

.. code-block:: c++

   invoke_result_t<Proj&, iter_value_t<It> &>

The projection is now constrained as a function adaptor, and we are back in the good block:

.. code-block:: c++

   iter_value_t<I> x = *it;
   f(proj(x));

|

----

Summary
=======

- Indirect callable concepts and `projected` have a problem!
- Fixed in an ABI-stable way by introducing `indirect_value_t`!
- Breaks API by relaxing constraints of algorithms!

----

Questions
=========
