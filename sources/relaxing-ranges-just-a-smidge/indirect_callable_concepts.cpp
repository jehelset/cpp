//ins
template< class F, class I >
  concept indirectly_regular_unary_reference_invocable =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    regular_invocable<F&, iter_reference_t<I>>;

//del
template< class F, class I >
  concept indirectly_regular_unary_invocable =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    regular_invocable<F&, iter_value_t<I>&> &&
    regular_invocable<F&, iter_reference_t<I>> &&
    regular_invocable<F&, iter_common_reference_t<I>> &&
    common_reference_with<
      invoke_result_t<F&, iter_value_t<I>&>,
      invoke_result_t<F&, iter_reference_t<I>>>;

//ins
template< class F, class I >
  concept indirectly_regular_unary_invocable =
    indirectly_regular_reference_invocable<F, I> &&
    regular_invocable<F&, iter_value_t<I>&> &&
    regular_invocable<F&, iter_common_reference_t<I>> &&
    common_reference_with<
      invoke_result_t<F&, iter_value_t<I>&>,
      invoke_result_t<F&, iter_reference_t<I>>>;

template<class I>
struct indirect_value{
  using type = iter_value_t<I> &;
};

template<indirectly_readable I>
using indirect-value-t = see below; // exposition only

//del
template< class F, class I >
  concept indirectly_unary_invocable =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    invocable<F&, iter_value_t<I>&> &&
    invocable<F&, iter_reference_t<I>> &&
    invocable<F&, iter_common_reference_t<I>> &&
    common_reference_with<
      invoke_result_t<F&, iter_value_t<I>&>,
      invoke_result_t<F&, iter_reference_t<I>>>;

//ins
template< class F, class I >
  concept indirectly_unary_invocable =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    invocable<F&, indirect_value_t<I>> &&
    invocable<F&, iter_reference_t<I>> &&
    invocable<F&, iter_common_reference_t<I>> &&
    common_reference_with<
      invoke_result_t<F&, indirect_value_t<I>>,
      invoke_result_t<F&, iter_reference_t<I>>>;

template<class F, class... Is>
  requires (indirectly_readable<Is> && ...) && invocable<F, iter_reference_t<Is>...>
    using indirect_result_t = invoke_result_t<F, iter_reference_t<Is>...>;

//del
template<class F, class I>
  concept indirectly_regular_unary_invocable =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    regular_invocable<F&, iter_value_t<I>&> &&
    regular_invocable<F&, iter_reference_t<I>> &&
    regular_invocable<F&, iter_common_reference_t<I>> &&
    common_reference_with<
      invoke_result_t<F&, iter_value_t<I>&>,
      invoke_result_t<F&, iter_reference_t<I>>>;


//ins
template<class F, class I>
  concept indirectly_regular_unary_invocable =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    regular_invocable<F&, indirect_value_t<I>> &&
    regular_invocable<F&, iter_reference_t<I>> &&
    regular_invocable<F&, iter_common_reference_t<I>> &&
    common_reference_with<
      invoke_result_t<F&, indirect_value_t<I>>,
      invoke_result_t<F&, iter_reference_t<I>>>;

//del
template<class F, class I>
  concept indirect_unary_predicate =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    predicate<F&, iter_value_t<I>&> &&
    predicate<F&, iter_reference_t<I>> &&
    predicate<F&, iter_common_reference_t<I>>;

//ins
template<class F, class I>
  concept indirect_unary_predicate =
    indirectly_readable<I> &&
    copy_constructible<F> &&
    predicate<F&, indirect_value_t<I>> &&
    predicate<F&, iter_reference_t<I>> &&
    predicate<F&, iter_common_reference_t<I>>;

//del
template<class F, class I1, class I2>
  concept indirect_binary_predicate =
    indirectly_readable<I1> && indirectly_readable<I2> &&
    copy_constructible<F> &&
    predicate<F&, iter_value_t<I1>&, iter_value_t<I2>&> &&
    predicate<F&, iter_value_t<I1>&, iter_reference_t<I2>> &&
    predicate<F&, iter_reference_t<I1>, iter_value_t<I2>&> &&
    predicate<F&, iter_reference_t<I1>, iter_reference_t<I2>> &&
    predicate<F&, iter_common_reference_t<I1>, iter_common_reference_t<I2>>;

//ins
template<class F, class I1, class I2>
  concept indirect_binary_predicate =
    indirectly_readable<I1> && indirectly_readable<I2> &&
    copy_constructible<F> &&
    predicate<F&, indirect_value_t<I1>, indirect_value_t<I1>> &&
    predicate<F&, indirect_value_t<I1>, iter_reference_t<I2>> &&
    predicate<F&, iter_reference_t<I1>, indirect_value_t<I1>> &&
    predicate<F&, iter_reference_t<I1>, iter_reference_t<I2>> &&
    predicate<F&, iter_common_reference_t<I1>, iter_common_reference_t<I2>>;

//del
template<class F, class I1, class I2 = I1>
  concept indirect_equivalence_relation =
    indirectly_readable<I1> && indirectly_readable<I2> &&
    copy_constructible<F> &&
    equivalence_relation<F&, iter_value_t<I1>&, iter_value_t<I2>&> &&
    equivalence_relation<F&, iter_value_t<I1>&, iter_reference_t<I2>> &&
    equivalence_relation<F&, iter_reference_t<I1>, iter_value_t<I2>&> &&
    equivalence_relation<F&, iter_reference_t<I1>, iter_reference_t<I2>> &&
    equivalence_relation<F&, iter_common_reference_t<I1>, iter_common_reference_t<I2>>;

//ins
template<class F, class I1, class I2 = I1>
  concept indirect_equivalence_relation =
    indirectly_readable<I1> && indirectly_readable<I2> &&
    copy_constructible<F> &&
    equivalence_relation<F&, indirect_value_t<I1>, indirect_value_t<I2>> &&
    equivalence_relation<F&, indirect_value_t<I1>, iter_reference_t<I2>> &&
    equivalence_relation<F&, iter_reference_t<I1>, indirect_value_t<I2>> &&
    equivalence_relation<F&, iter_reference_t<I1>, iter_reference_t<I2>> &&
    equivalence_relation<F&, iter_common_reference_t<I1>, iter_common_reference_t<I2>>;

//del 
template<class F, class I1, class I2 = I1>
  concept indirect_strict_weak_order =
    indirectly_readable<I1> && indirectly_readable<I2> &&
    copy_constructible<F> &&
    strict_weak_order<F&, iter_value_t<I1>&, iter_value_t<I2>&> &&
    strict_weak_order<F&, iter_value_t<I1>&, iter_reference_t<I2>> &&
    strict_weak_order<F&, iter_reference_t<I1>, iter_value_t<I2>&> &&
    strict_weak_order<F&, iter_reference_t<I1>, iter_reference_t<I2>> &&
    strict_weak_order<F&, iter_common_reference_t<I1>, iter_common_reference_t<I2>>;

//ins
template<class F, class I1, class I2 = I1>
  concept indirect_strict_weak_order =
    indirectly_readable<I1> && indirectly_readable<I2> &&
    copy_constructible<F> &&
    strict_weak_order<F&, indirect_value_t<I1>, indirect_value_t<I2>> &&
    strict_weak_order<F&, indirect_value_t<I1>, iter_reference_t<I2>> &&
    strict_weak_order<F&, iter_reference_t<I1>, indirect_value_t<I2>> &&
    strict_weak_order<F&, iter_reference_t<I1>, iter_reference_t<I2>> &&
    strict_weak_order<F&, iter_common_reference_t<I1>, iter_common_reference_t<I2>>;
