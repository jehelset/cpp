<pre class='metadata' highlight=cpp>
Title: Relaxing Ranges Just A Smidge
Shortname: DXXXX
Revision: 0
Audience: SG9
Status: D
Group: WG21
!Source: <a href="https://gitlab.com/jehelset/cpp/-/blob/main/sources/relaxing-ranges-just-a-smidge/D0.bs">https://gitlab.com/jehelset/cpp/-/blob/main/sources/relaxing-ranges-just-a-smidge/D0.bs</a>
URL: https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/D0.html
Editor: John Eivind Helset, private@jehelset.no
Date: 2022-04-17
Markup Shorthands: markdown yes
No abstract: true
Toggle Diffs: yes
</pre>

<style>
table, th, td { border:1px solid grey; }
</style>

Introduction {#introduction}
============

Ranges is filled to the brim with useful algorithms. Their constraints and associated concepts
helped me, an avid ranges user, learn how to use the ranges library. Unfortunately the constraints
related to invocability of functions and projections that the user passes to the algorithms can be
confusing. Two types of particularly confusing constraints haunt even the the simplest of
algorithms; `for_each` and `for_each_n`. This proposal seeks to simply demonstrate the issue,
and discuss potential solutions, but does itself not actually propose any particular solution.

Unnecessary invocability {#introduction-unnecessary_invocability}
------------------------

In the discussion of `ranges::copy_unique` in [[Niebler2015]], Eric Niebler explains that the
constraint put on the comparator by `ranges::unique_copy` must be strong enough to allow the
algorithm to invoke with a reference formed to a copy if it needs to. The current iteration of
`ranges::unique_copy` also involves a projection, and is specified as:
<pre>
template&lt;input_range R, weakly_incrementable O, class Proj = identity,
         indirect_equivalence_relation&lt;projected&lt;iterator_t&lt;R&gt;, Proj&gt;&gt; C = ranges::equal_to&gt;
  requires indirectly_copyable&lt;iterator_t&lt;R&gt;, O&gt; &amp;&amp;
           (forward_iterator&lt;iterator_t&lt;R&gt;&gt; ||
            (input_iterator&lt;O&gt; &amp;&amp; same_as&lt;range_value_t&lt;R&gt;, iter_value_t&lt;O&gt;&gt;) ||
            indirectly_copyable_storable&lt;iterator_t&lt;R&gt;, O&gt;)
  constexpr ranges::unique_copy_result&lt;borrowed_iterator_t&lt;R&gt;, O&gt;
    ranges::unique_copy(R&amp;&amp; r, O result, C comp = {}, Proj proj = {});
</pre>

The constraint promises to the user that it will only copy elements in the case where the user
passes an input range whose value-type differs from the value-type of the output iterator. Yet it
unequivocally demands that the projection the user supplies is invocable with a reference to a
copied element. If passed a forward range, `ranges::unique_copy` would demand a form of invocability
which seem contrary to its own constraints. It is promising to not to do `auto v = *it;`, yet it
requires `proj(v)`. Even when the code that required copies was discarded at compile-time, and is
some sense, not part of the algorithm that the user chose.

Let's see how this constraint is implemented. The projection, `Proj`, is constrained via the
constraints put on the template parameter of `projected`:
<pre>
template&lt;indirectly_readable I, indirectly_regular_unary_invocable&lt;I&gt; Proj&gt;
struct projected {
  using value_type = remove_cvref_t&lt;indirect_result_t&lt;Proj&amp;, I&gt;&gt;;
  indirect_result_t&lt;Proj&amp;, I&gt; operator*() const; // not defined
};
</pre>
The projection must be `indirectly_regular_unary_invocable` with the iterator
`I`, which in turn means:
<pre>
template&lt;class F, class I&gt;
  concept indirectly_regular_unary_invocable =
    indirectly_readable&lt;I&gt; &amp;&amp;
    copy_constructible&lt;F&gt; &amp;&amp;
    regular_invocable&lt;F&amp;, iter_value_t&lt;I&gt;&amp;&gt; &amp;&amp;
    regular_invocable&lt;F&amp;, iter_reference_t&lt;I&gt;&gt; &amp;&amp;
    regular_invocable&lt;F&amp;, iter_common_reference_t&lt;I&gt;&gt; &amp;&amp;
    common_reference_with&lt;
      invoke_result_t&lt;F&amp;, iter_value_t&lt;I&gt;&amp;&gt;,
      invoke_result_t&lt;F&amp;, iter_reference_t&lt;I&gt;&gt;&gt;;
</pre>

The constraint `regular_invocable<F&, iter_value_t<I>&>` expresses invocability with an element that
was copied, or moved, out of the range. This is quite subtle, and not well-documented as far as I
can tell.

I will refer to indirect invocability with the iterator's reference as reference-invocability, and
indirect invocability with a reference to the iterator's value-type as value-invocability. The
algorithms that only use the references of an iterator into a range will be referred to as
referencing algorithms, while those that also copy or move values out of the range will be referred
to as copying algorithms (even though they don't necessarily need to copy).

It is quite easy to make this constraint fail. Below is a failed attempt at using one of the simplest
views with one of the simplest algorithms:
<pre>
#include &lt;algorithm&gt;
#include &lt;iostream&gt;
#include &lt;ranges&gt;

int main(){
  std::ranges::for_each(
    std::views::iota(0, 5),
    [](int &amp;&amp;v){
      std::cout &lt;&lt; v &lt;&lt; std::endl;
    },
    [](int &amp;&amp;i){
        return i + 2;
    });
}
</pre>

In a sense, the programmer is trying to be too precise for the algorithm's liking. It is interesting
to compare the exposition of the problem so far, with the compiler error that the program produces
in this [godbolt](https://godbolt.org/z/eWeGq5ecK).  

It is my opinion that the algorithms in ranges should not require forms of invocability they
themselves are not constrained to invoke. The subtlety of how these constraints are represented in
code, the complexity of the associated concepts, the sparsity of documentation of said concepts, the
ease and manner in which a user would accidentally violate these constraints, and the resulting
compiler diagnostics, together, make me think that these constraints should be relaxed wherever
possible. A referencing algorithm should only require a reference-invocable projection, nor should
they require that the functions are invocable with the projection of a copied value. The referencing
and copying instantiations of for example `ranges::unique_copy` should be considered different
algorithms in this respect.

Nonsensical invocability {#introduction-nonsensical_invocability}
------------------------

The second problematic form of invocability is decidedly more confusing than the first. This form is
required from the functions, and not the projections, that are passed to algorithms. The simple
example in [[#introduction-unnecessary_invocability]] would have required this form of invocability,
and failed, had it not taken the easy way out via the constraint on the projection.

In [[Revzin2022]], Barry Revzin writes that projections are function adaptors, and do not change an
algorithm. In the unary case, projection is reduced to composition. Instead of `f` there's now
`compose(f,proj)`. This would imply that the different invocations of referencing and copying
algorithms, with and without a projection are:

<table>
<tr><th>Algorithm</th><th>Invocation</th><th>Adapted Invocation</th></tr>
<tr><td>Referencing</td><td><pre>f(*it)</pre></td><td><pre>f(proj(*it))</pre></td></tr>
<tr>
  <td>Copying</td>
  <td>
    <pre>
      auto v = *it;
      f(v);
    </pre>
  </td>
  <td>
    <pre>
      auto v = *it;
      f(proj(v));
    </pre>
  </td>
</tr>
</table>

I was surprised to learn that the adapted invocation shown here in the copying case is not what the
standard library is requiring. Let's see what it actually requires by revisiting `ranges::for_each`:
<pre>
template&lt;input_range R, class Proj = identity,
         indirectly_unary_invocable&lt;projected&lt;iterator_t&lt;R&gt;, Proj&gt;&gt; Fun&gt;
constexpr ranges::for_each_result&lt;borrowed_iterator_t&lt;R&gt;, Fun&gt;
  ranges::for_each(R&amp;&amp; r, Fun f, Proj proj = {});
</pre>

It requires a function which is `indirectly_unary_invocable` with the `projected` of the
iterator. As mentioned, this concept unequivocally requires value-invocable, which was expressed
by invocability with `ranges::iter_value_t<I> &`. For `projected` that means
`remove_cvref_t<indirect_result_t<Proj &,I>>;`, which seems to indicate that the algorithm wants to
do:

<pre>
  auto u = proj(*it);
  f(u);
</pre>

None of the algorithms in ranges are constrained to support such an invocation. The attentive reader
might have noticed how `ranges::unique_copy`, which was shown earlier, in the copying case, is
constrained according to `indirectly_copyable_storable<iterator_t<R>, O>`, and not
`indirectly_copyable_storable<projected<iterator_t<R>, Proj>, O>>`.

Even if an algorithm was constrained in such a way, there is no guarantee that the validity of the
projection is not coupled to the lifetime of the element. To put it in ranges terms, the projection
is not necessarily borrowable with respect to the element. Let's see what would happen if an
algorithm like `ranges::max` was constrained to copy projections, and copied the projection instead
of the element. It's body might then look something like:
<pre>
  auto i = ranges::begin(r);
  auto s = ranges::end(r);
  auto m = proj(*i);
  while (++i != s) {
    if (comp(m, proj(*i))) {
      m = proj(*i);
    }
  }
  return m;
</pre>

If passed an input range and a projection that returns a proxy, say `reference_wrapper`, to some
subobject of the element, by the time the algorithm reaches `comp(m,proj(*i))`, the object referred
to by `m` might have already ceased to exist. The algorithm would need to handle this case in some
way or another, for example by also storing the element, or maybe somehow prohibiting the user from
passing such combinations of ranges and projections.

This form of invocability seems nonsensical to me. Instead the triplet of iterator, function, and
projection should be constrained to enable adapted reference- and value-invocability as shown in the
table above. I will refer to these adapted forms of invocation as projected-reference-invocation,
and projected-value-invocation, with corresponding invocability-concepts.

Unfortunately it is even simpler to write a program that triggers a violation of this constraint. 
All that is needed is to remove the projection from the previous program:
<pre>
#include&lt;algorithm&gt;
#include&lt;iostream&gt;
#include&lt;ranges&gt;

int main(){
  std::ranges::for_each(
    std::views::iota(0, 5),
    [](int &amp;&amp;v){
      std::cout &lt;&lt; v &lt;&lt; std::endl;
    });
}
</pre>

Again, the programmer is trying to be too precise, and again we note the error message that the
compiler produces in this [godbolt](https://godbolt.org/z/z6hdKx6vM).

Impact on standard {#introduction-impact_on_standard}
------------------

These two types of constraints are quite common in ranges. Every unconditionally referencing, or
conditionally copying algorithm, that takes a projection, requires unnecessary invocability. Every
algorithm that takes a projection requires nonsensical invocability. The saving grace, in the latter
case, is that the syntactic expression of nonsensical invocability is often naturally covered, for
example due to ranges whose reference-types happen to be references, or forwarding functions, or
functions that take parameters by value in conjunction with copyable value-types, or referencing
projections.

Proposal {#proposal}
========

This proposal is similar in some ways to [[P2550R0]]. Both aim at reformulating constraints and
concepts to make them easier to understand, relax concepts and algorithms by defining strong and
weak forms of existing concepts to reconstraint algorithm. I believe work in this direction would
make ranges both more powerful and easier to understand and use for the C++ community at
large. 

Unecessary and nonsensical invocability are consequences of how the triplet of iterator, function,
and projection is constrained. Recall that `indirect_unary_invocable<projected<I, Proj>> F`
constrains both the callable `F` with the binary constraint `indirect_unary_invocable`, but also the
projection `Proj` with the binary constraint `regular_indirect_unary_invocable`. It is using the
same constraint (modulo regular) to express value-invocability, and projected-value-invocability,
which are actually semantically and syntactically two very different constraints. The failure to
account for this difference is the source of the problematic forms of
invocability. `regular_indirect_unary_invocable` unconditionally requires a value-invocable
projection. `regular_unary_invocable` also unconditionally requires something value-invocable, and
the composition of `indirect_unary_invocable` and `projected` results in unecessary and non-sensical
value-invocability. As mentioned earlier, depending on whether or not the algorithm is referencing
or copying, the projection should intead be constrained with either reference- or
value-invocability, and the function should be constrained with projected reference- or
value-invocability.

I believe the solution to these problems is to define and use ternary concepts that constrain the
triplet of iterator, function, and projection together, and have sensible and clearly expressed
semantics. This will require that a projection parameter is added to existing concepts, in addition
to splitting them up into a strong and weak form. For a referencing algorithm one would constrain
with `indirect_unary_reference_invocable<I, Proj> F` instead of
`indirectly_unary_invocable<projected<I, Proj>> F`. If adding a new parameter to existing constraint
was out of the question, one could perhaps instead define new concepts, like
`projected_reference_invocable<I, Proj> F`.

Even though this approach would solve the problem, it is invasive and requires quite a few new
additions and modifications to the standard library, and might lead to an unrealistic proposal with
a low chance of adoption. At the very least any indirect-callable concepts would need to be made
ternary and split up into a strong and weak form. The common algorithm requirements that use
`projected` would need to be redefined to use the ternary concepts, and the algorithms that use
`projected` would need to be reconstrained to use the correct ternary concept instead. Referencing
algorithms use the weak form and copying algorithms use the strong form. On the other hand
`projected` would not longer serve any purpose.

These are quite invasive changes, and add a lot of new concepts to the standard. It is more
important to make the algorithms easier to use by relaxing their syntactic constraints, than to make
the precise semantics of said constraints sensible and easy to understand. The user's compiler only
complains about syntax, and not semantics. However I don't currently see a good non-invasive way to
solve this problem, as the `indirect_callable_concept<projected<I, Proj>>` construction seems
fundamentally flawed.

Wording {#wording}
=======

Acknowledgements {#acknoweldgements}
================

The author would like to thank Barry Rezvin and Christopher Di Bella for guidance.

<pre class=biblio>
{
"P2550R0": {
  "title": "ranges::copy should say output_iterator somewhere",
  "authors": ["Barry Revzin"],
  "href": "http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2550r0.html"
},
"Revzin2022": {
  "title": "Projections are Function Adaptors",
  "authors": ["Barry Revzin"],
  "href": "https://brevzin.github.io/c++/2022/02/13/projections-function-adaptors/"
},
"Niebler2015": {
  "title": "Iterators++, Part 3",
  "authors": ["Eric Niebler"],
  "href": "https://ericniebler.com/2015/03/03/iterators-plus-plus-part-3/"
}
}
</pre>
