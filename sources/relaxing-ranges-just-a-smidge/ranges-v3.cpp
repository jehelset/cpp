template<typename I, typename Proj>
struct projected_
{
  struct type
  {
    using reference = indirect_result_t<Proj &, I>;
    using value_type = uncvref_t<reference>;
    reference operator*() const;

    using indirect_value_type = invoke_result_t<Proj &, iter_value_t<I> &>;
  };
};

template<typename T>
requires requires{ typename T::indirect_value_type; }
struct indirect_value<T>
{
  using type = typename T::indirect_value_type;
};

template<typename I>
struct indirect_value
{
  using type = iter_value_t<I> &;
};
template<typename I>
using indirect_value_t = typename indirect_value<I>::type;

