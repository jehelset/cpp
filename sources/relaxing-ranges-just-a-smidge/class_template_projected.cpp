template<indirectly_readable I, indirectly_regular_unary_reference_invocable<I> Proj>
struct reference_projected {
  using value_type = invoke_result_t<Proj, iter_reference_t<I>> &&;
  indirect_result_t<Proj&, I> operator*() const; // not defined
};

template<weakly_incrementable I, class Proj>
struct incrementable_traits<reference_projected<I, Proj>> {
  using difference_type = iter_difference_t<I>;
};

template<indirectly_readable I, indirectly_regular_unary_invocable<I> Proj>
struct value_projected {
  using value_type = invoke_result_t<Proj, iter_value_t<I> &> &&;
  indirect_result_t<Proj&, I> operator*() const; // not defined
};

template<indirectly_readable I, indirectly_regular_unary_invocable<I> Proj>
struct projected {
  using value_type = invoke_result_t<Proj, iter_value_t<I> &> &&;
  indirect_result_t<Proj&, I> operator*() const; // not defined
};

template<class I>
requires requires{ typename I::indirect-value-type; } 
struct indirect_value<projected<I, Proj>>{
  using type = typename I::indirect-value-type; 
};

template<indirectly_readable I, indirectly_regular_unary_invocable<I> Proj>
struct projected {
  using value_type = remove_cvref_t<indirect_result_t<Proj&, I>>;
  indirect_result_t<Proj&, I> operator*() const; // not defined
};

template<indirectly_readable I, indirectly_regular_unary_invocable<I> Proj>
struct projected {
  using value_type = invoke_result_t<Proj, iter_value_t<I> &> &&;
  indirect_result_t<Proj&, I> operator*() const; // not defined

  using indirect-value-type = invoke_result_t<Proj &, iter_value_t<Iter> &>; // exposition only
};

