//del
template<input_iterator I, sentinel_for<I> S, class Proj = identity,
	 indirectly_unary_invocable<projected<I, Proj>> Fun>
constexpr ranges::for_each_result<I, Fun>
  ranges::for_each( I first, S last, Fun f, Proj proj = {} );
template<ranges::input_range R, class Proj = identity,
	 indirectly_unary_invocable<projected<ranges::iterator_t<R>, Proj>> Fun >
constexpr ranges::for_each_result<ranges::borrowed_iterator_t<R>, Fun>
  ranges::for_each( R&& r, Fun f, Proj proj = {} );
//ins
template<input_iterator I, sentinel_for<I> S, class Proj = identity,
	 indirectly_unary_invocable<reference_projected<I, Proj>> Fun>
constexpr ranges::for_each_result<I, Fun>
  ranges::for_each( I first, S last, Fun f, Proj proj = {} );
template<ranges::input_range R, class Proj = identity,
	 indirectly_unary_invocable<reference_projected<ranges::iterator_t<R>, Proj>> Fun>
constexpr ranges::for_each_result<ranges::borrowed_iterator_t<R>, Fun>
  ranges::for_each( R&& r, Fun f, Proj proj = {} );
//del
template<input_iterator I, class Proj = identity,
	 indirectly_unary_invocable<projected<I, Proj>> Fun >
constexpr ranges::for_each_n_result<I, Fun>
  ranges::for_each_n( I first, iter_difference_t<I> n, Fun f, Proj proj = {});
//ins
template<input_iterator I, class Proj = identity,
	 indirectly_unary_invocable<reference_projected<ranges::iterator_t<R>, Proj>> Fun>
constexpr ranges::for_each_n_result<I, Fun>
  ranges::for_each_n( I first, iter_difference_t<I> n, Fun f, Proj proj = {});
