((nil
  (eval
   .
   (if
       (not (boundp 'cpp-loaded))
       (progn
         (setenv "PATH" (string-join `(,"~/.local/bin"
                                       ,(getenv "PATH"))
                                     ":"))
	 (setq yas-snippet-dirs (list (concat (projectile-project-root) "snippets")))
         (setq projectile-project-compilation-cmd "bikeshed")
         (setq projectile-project-run-cmd "bikeshed serve --port 10000")
         (setq cpp-loaded 1))))))
