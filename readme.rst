C++ papers
##########

Relaxing Ranges Just A Smidge
-----------------------------

- `D0 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/D0.html>`_
- `D1 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/D1.html>`_
- `D2 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/D2.html>`_
- `D2 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/D3.html>`_
- `P0 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/P0.html>`_
- `P1 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/P1.html>`_
- `P2 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/P2.html>`_
- `P3 <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge/P3.html>`_
- `latest <https://jehelset.gitlab.io/cpp/relaxing-ranges-just-a-smidge>`_

`std::views::ref`
-----------------

- `latest <https://jehelset.gitlab.io/cpp/std-views-ref>`_
